<?php

namespace Enquetes\Http\Controllers;
use Enquetes\Models\Voto;
use Enquetes\Http\Controllers\AppBaseController;
use Enquetes\Http\Requests\CreateEnqueteRequest;
use Enquetes\Http\Requests\UpdateEnqueteRequest;
use Enquetes\Repositories\EnqueteRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class EnqueteController extends AppBaseController
{
    /** @var  EnqueteRepository */
    private $enqueteRepository;

    public function __construct(EnqueteRepository $enqueteRepo)
    {
        $this->enqueteRepository = $enqueteRepo;
    }

    /**
     * Display a listing of the Enquete.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->enqueteRepository->pushCriteria(new RequestCriteria($request));
        $enquetes = $this->enqueteRepository->getmodelObj()->where('usuario_id','=',auth()->id())->paginate(10);
         
        return view('enquetes.index')
            ->with('enquetes', $enquetes);
    }

    /**
     * Show the form for creating a new Enquete.
     *
     * @return Response
     */
    public function create()
    {
        return view('enquetes.create');
    }

    /**
     * Store a newly created Enquete in storage.
     *
     * @param CreateEnqueteRequest $request
     *
     * @return Response
     */
    public function store(CreateEnqueteRequest $request)
    {
        $input = $request->all();
        $input['usuario_id'] = Auth::id();
        $input['ativo'] = 1;
        $enquete = $this->enqueteRepository->create($input);
        // Session::put('enquete_id', $enquete->id);
        Flash::success('Enquete salva.');

        return redirect(route('respostas.enqueteResposta',$enquete->id));
    }

    /**
     * Display the specified Enquete.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $enquete = $this->enqueteRepository->findWithoutFail($id);
        $grafico = "[['Resposta','Votos']";
        foreach($enquete->enquetesRespostas as $resposta) {
            $qtd_voto = Voto::where('enquete_id',$enquete->id)->where('resposta_id',$resposta->id)->count();
                $grafico .= ",["."'".$resposta->resposta."'".",".$qtd_voto."]";
        }
        $grafico .= ']';
      
        if (empty($enquete)) {
            Flash::error('Enquete não encontrada');
            return redirect(route('enquetes.index'));
        }
        return view('enquetes.show',compact('grafico'))->with('enquete', $enquete);
    }

    /**
     * Show the form for editing the specified Enquete.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $enquete = $this->enqueteRepository->findWithoutFail($id);
        if (empty($enquete)) {
            Flash::error('Enquete não encontrada');

            return redirect(route('enquetes.index'));
        }

        return view('enquetes.edit')->with('enquete', $enquete);
    }

    /**
     * Update the specified Enquete in storage.
     *
     * @param  int              $id
     * @param UpdateEnqueteRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [              
                'titulo' => 'required',
                'descricao' => 'required',
                'data_inicio' => 'required',
                'slug'=> [
                        'required',
                        'alpha_dash',
                        'min:5',
                        'max:255',
                        'unique:enquetes,slug,'.$id
                ]
            ]);  
        
        $enquete = $this->enqueteRepository->findWithoutFail($id);

        if (empty($enquete)) {
            Flash::error('Enquete não encontrada');

            return redirect(route('enquetes.index'));
        }

        $enquete = $this->enqueteRepository->update($request->all(), $id);

        Flash::success('Enquete atualizada.');

        return redirect(route('respostas.enqueteResposta',$enquete->id));
    }

    /**
     * Remove the specified Enquete from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $enquete = $this->enqueteRepository->findWithoutFail($id);

        if (empty($enquete)) {
            Flash::error('Enquete não encontrada');

            return redirect(route('enquetes.index'));
        }

        $this->enqueteRepository->delete($id);

        Flash::success('Enquete deletada.');

        return redirect(route('enquetes.index'));
    }
}
