<?php

namespace Enquetes\Http\Controllers;

use Enquetes\Http\Controllers\AppBaseController;
use Enquetes\Http\Requests\CreateRespostaRequest;
use Enquetes\Http\Requests\UpdateRespostaRequest;
use Enquetes\Models\Enquete;
use Enquetes\Repositories\RespostaRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RespostaController extends AppBaseController
{
    /** @var  RespostaRepository */
    private $respostaRepository;

    public function __construct(RespostaRepository $respostaRepo)
    {
        $this->respostaRepository = $respostaRepo;
    }

   

    public function enqueteResposta($id) {
        $enquete = Enquete::find($id);
        $respostas = $enquete->enquetesRespostas;
        return view('respostas.create',compact('enquete','respostas'));
    }

    /**,,,,,,,,,,,,,
     * Show the form for creating a new Resposta.
     *
     * @return Response
     */
    public function create()
    {
        $enquete = Enquete::find(Session::get('enquete_id'));
        $respostas = $enquete->enquetesRespostas;
        return view('respostas.create',compact('enquete','respostas'));
    }

    /**
     * Store a newly created Resposta in storage.
     *
     * @param CreateRespostaRequest $request
     *
     * @return Response
     */
    public function store(CreateRespostaRequest $request)
    {
        $input = $request->all();

        $resposta = $this->respostaRepository->create($input);

        Flash::success('Resposta incluida.');

        return redirect(route('respostas.enqueteResposta',$input['enquete_id']));
    }

    /**
     * Remove the specified Resposta from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $resposta = $this->respostaRepository->findWithoutFail($id);
        $enquete_id = $resposta->enquete_id;
        if (empty($resposta)) {
            Flash::error('Resposta not found');

            return redirect(route('respostas.index'));
        }

        $this->respostaRepository->delete($id);

        Flash::success('Resposta deletada.');

        return redirect(route('respostas.enqueteResposta',$enquete_id));
    }
}
