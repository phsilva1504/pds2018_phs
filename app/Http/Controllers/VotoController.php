<?php

namespace Enquetes\Http\Controllers;

use Enquetes\Http\Requests\CreateVotoRequest;
use Enquetes\Http\Requests\UpdateVotoRequest;
use Enquetes\Repositories\VotoRepository;
use Enquetes\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class VotoController extends AppBaseController
{
    /** @var  VotoRepository */
    private $votoRepository;

    public function __construct(VotoRepository $votoRepo)
    {
        $this->votoRepository = $votoRepo;
    }



    /**
     * Store a newly created Voto in storage.
     *
     * @param CreateVotoRequest $request
     *
     * @return Response
     */
    public function store(CreateVotoRequest $request)
    {
        
        $input = $request->all();

        $voto = $this->votoRepository->create($input);

        Flash::success('Voto computado.');

        return redirect(route('blog.single', $request->slug));
    }

}
