<?php

namespace Enquetes\Http\Controllers;

use Enquetes\Models\Enquete;
use Enquetes\Models\Voto;
class HomeController extends Controller
{
    public function __construct()
    {
         
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
       
        $enquetes = Enquete::where('usuario_id',auth()->id())->get();
        $grafico = "[['Enquete','Votos']";
        foreach($enquetes as $enquete) {
                $grafico .= ",["."'".$enquete->titulo."'".",".$enquete->votos->count()."]";
        }
        $grafico .= ']';
        $check = $grafico == "[['Enquete','Votos']]" ? false : true;
        return view('home',compact('grafico','check'));
    }

    public function blog() {
        date_default_timezone_set('America/Sao_Paulo');
        
         $now = date('Y-m-d');
         $enquetes = Enquete::where([
                                        ['data_inicio','<=',$now],
                                        ['data_fim','>=',$now],
                                        ['ativo','=',1]    
                                    ])->orderBy('created_at', 'desc')->paginate(3);
         return view('votacao.index')
            ->with('enquetes', $enquetes);        
    }

      public function getSingle($slug) {
         $enquete = Enquete::where('slug', '=', $slug)->first();
         $total = $enquete->votos->count();
         $respostas = array();
         foreach($enquete->enquetesRespostas->toArray() as $resposta){
            $count_votos = Voto::where('enquete_id',$enquete->id)->where('resposta_id',$resposta['id'])->count();
            $resposta['percentual'] = $total > 0 ? number_format(($count_votos/$total)*100,2) : 0;
            $respostas[] = $resposta;
         }         
         return view('votacao.post',compact('slug','respostas'))
            ->with('enquete', $enquete);
    }
}
