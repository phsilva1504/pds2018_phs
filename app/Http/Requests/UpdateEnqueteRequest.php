<?php

namespace Enquetes\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Enquetes\Models\Enquete;

class UpdateEnqueteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'titulo' => 'required',
            'descricao' => 'required',
            'data_inicio' => 'required',
            'slug'=> ['required',
                        'alpha_dash',
                        'min:5',
                        'max:255',
                        'unique:enquetes,slug,'.$this->id
                    ]

        ];
    }
}
