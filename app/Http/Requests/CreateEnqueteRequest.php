<?php

namespace Enquetes\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Enquetes\Models\Enquete;

class CreateEnqueteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Enquete::$rules;
    }

    
}
