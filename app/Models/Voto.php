<?php

namespace Enquetes\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Voto
 * @package Enquetes\Models
 * @version October 11, 2018, 11:56 pm UTC
 *
 * @property \Enquetes\Models\Enquete enquete
 * @property \Enquetes\Models\EnquetesResposta enquetesResposta
 * @property integer enquete_id
 * @property integer resposta_id
 */
class Voto extends Model
{
    // use SoftDeletes;

    public $table = 'votos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'enquete_id',
        'resposta_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'enquete_id' => 'integer',
        'resposta_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enquete()
    {
        return $this->belongsTo(\Enquetes\Models\Enquete::class,'enquete_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function resposta()
    {
        return $this->belongsTo(\Enquetes\Models\EnquetesResposta::class,'resposta_id');
    }
}
