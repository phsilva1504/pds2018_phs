<?php

namespace Enquetes\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Resposta
 * @package Enquetes\Models
 * @version September 21, 2018, 12:19 am UTC
 *
 * @property \Enquetes\Models\Enquete enquete
 * @property \Illuminate\Database\Eloquent\Collection Voto
 * @property string resposta
 * @property integer enquete_id
 */
class Resposta extends Model
{
    // use SoftDeletes;

    public $table = 'enquetes_respostas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'resposta',
        'enquete_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resposta' => 'string',
        'enquete_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resposta' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enquete()
    {
        return $this->belongsTo(\Enquetes\Models\Enquete::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function votos()
    {
        return $this->hasMany(\Enquetes\Models\Voto::class);
    }
}
