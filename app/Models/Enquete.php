<?php

namespace Enquetes\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class Enquete
 * @package Enquetes\Models
 * @version September 21, 2018, 12:03 am UTC
 *
 * @property \Enquetes\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection EnquetesResposta
 * @property \Illuminate\Database\Eloquent\Collection Voto
 * @property string titulo
 * @property string descricao
 * @property date data_inicio
 * @property date data_fim
 * @property boolean ativo
 * @property integer usuario_id
 */
class Enquete extends Model
{
    // use SoftDeletes;

    public $table = 'enquetes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'slug',
        'descricao',
        'data_inicio',
        'data_fim',
        'ativo',
        'usuario_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titulo' => 'string',
        'descricao' => 'string',
        'data_inicio' => 'date',
        'data_fim' => 'date',
        'ativo' => 'boolean',
        'usuario_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'descricao' => 'required',
        'data_inicio' => 'required',
        'slug'=> 'required|alpha_dash|min:5|max:255|unique:enquetes,slug'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\Enquetes\User::class,'usuario_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function enquetesRespostas()
    {
        return $this->hasMany(\Enquetes\Models\Resposta::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function votos()
    {
        return $this->hasMany(\Enquetes\Models\Voto::class);
    }

    public function getDataInicioAttribute($value)
    {
        return date('Y-m-d',strtotime($value));
    }

    public function getDataFimAttribute($value)
    {
        return date('Y-m-d',strtotime($value));
    }
}
