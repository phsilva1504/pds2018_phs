<?php

namespace Enquetes\Repositories;

use Enquetes\Models\Resposta;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RespostaRepository
 * @package Enquetes\Repositories
 * @version September 21, 2018, 12:19 am UTC
 *
 * @method Resposta findWithoutFail($id, $columns = ['*'])
 * @method Resposta find($id, $columns = ['*'])
 * @method Resposta first($columns = ['*'])
*/
class RespostaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'resposta',
        'enquete_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Resposta::class;
    }

    public function getmodelObj()
    {
        return new Resposta();
    }
}
