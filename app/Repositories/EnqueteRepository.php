<?php

namespace Enquetes\Repositories;

use Enquetes\Models\Enquete;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EnqueteRepository
 * @package Enquetes\Repositories
 * @version September 21, 2018, 12:03 am UTC
 *
 * @method Enquete findWithoutFail($id, $columns = ['*'])
 * @method Enquete find($id, $columns = ['*'])
 * @method Enquete first($columns = ['*'])
*/
class EnqueteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'descricao',
        'data_inicio',
        'data_fim',
        'ativo',
        'usuario_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Enquete::class;
    }

    public function getmodelObj()
    {
        return new Enquete();
    }
}
