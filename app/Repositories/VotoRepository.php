<?php

namespace Enquetes\Repositories;

use Enquetes\Models\Voto;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VotoRepository
 * @package Enquetes\Repositories
 * @version October 11, 2018, 11:56 pm UTC
 *
 * @method Voto findWithoutFail($id, $columns = ['*'])
 * @method Voto find($id, $columns = ['*'])
 * @method Voto first($columns = ['*'])
*/
class VotoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'enquete_id',
        'resposta_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Voto::class;
    }
}
