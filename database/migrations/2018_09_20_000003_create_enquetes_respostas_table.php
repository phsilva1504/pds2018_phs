<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquetesRespostasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'enquetes_respostas';

    /**
     * Run the migrations.
     * @table enquetes_respostas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('resposta')->nullable();
            $table->unsignedInteger('enquete_id');
            $table->timestamps();
            $table->softDeletes();

            $table->index(["enquete_id"], 'fk_enquetes_respostas_enquetes1_idx');


            $table->foreign('enquete_id', 'fk_enquetes_respostas_enquetes1_idx')
                ->references('id')->on('enquetes')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
