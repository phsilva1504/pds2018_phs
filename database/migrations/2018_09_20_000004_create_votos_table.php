<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'votos';

    /**
     * Run the migrations.
     * @table votos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('enquete_id');
            $table->unsignedInteger('resposta_id');
            $table->timestamps();
            $table->softDeletes();

            $table->index(["enquete_id"], 'fk_votos_enquetes1_idx');

            $table->index(["resposta_id"], 'fk_votos_enquetes_respostas1_idx');


            $table->foreign('enquete_id', 'fk_votos_enquetes1_idx')
                ->references('id')->on('enquetes')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('resposta_id', 'fk_votos_enquetes_respostas1_idx')
                ->references('id')->on('enquetes_respostas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
