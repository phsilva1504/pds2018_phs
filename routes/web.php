<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('blog');
});


Auth::routes();
// Route::get('/votacao', 'VotacaoController@index');

Route::get('/home', 'HomeController@index');
Route::get('/blog', 'HomeController@blog');
Route::resource('enquetes', 'EnqueteController');

Route::get('respostas/enquetes/{id}', 'RespostaController@enqueteResposta')->name('respostas.enqueteResposta');
Route::resource('respostas', 'RespostaController',['only' => [
    'create', 'store' , 'destroy'
]]);

Route::get('/votar/{slug}', ['as' => 'blog.single', 'uses' => 'HomeController@getSingle'])->where('slug', '[\w\d\-\_]+');

Route::resource('votos', 'VotoController',['only' => [
   'store'
]]);