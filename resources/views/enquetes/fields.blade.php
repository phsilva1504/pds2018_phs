
   
 
       
            <!-- Titulo Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('titulo', 'Titulo:') !!}
                {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('slug', 'Slug:') !!}
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Descricao Field -->
            <div class="form-group col-sm-12 col-lg-12">
                {!! Form::label('descricao', 'Descricao:') !!}
                {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Data Inicio Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('data_inicio', 'Data Inicio:') !!}
                {!! Form::date('data_inicio', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Data Fim Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('data_fim', 'Data Fim:') !!}
                {!! Form::date('data_fim', null, ['class' => 'form-control']) !!}
            </div>
        




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('enquetes.index') !!}" class="btn btn-default">Voltar</a>
</div>
