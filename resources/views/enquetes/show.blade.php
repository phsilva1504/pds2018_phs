@extends('layouts.app')
@section('scripts')
    <script type="text/javascript">
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
        var data = google.visualization.arrayToDataTable({!! $grafico !!}) // CSS-style declaration
        // Set chart options
        var options = {'title':'{{$enquete->titulo}}',
                       'chartArea': {width: '50%'},
                       hAxis: {
                          title: 'Votos',
                          minValue: 0
                        }
                   };
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Enquete
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div align="center">
                        <div id="chart_div"></div>
                    </div>
                    <a href="{!! route('enquetes.index') !!}" class="btn btn-default">Voltar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
