@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Enquete
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($enquete->toArray(), ['route' => ['enquetes.update', $enquete->id], 'method' => 'patch']) !!}

                        @include('enquetes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection