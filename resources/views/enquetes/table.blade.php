<table class="table table-responsive" id="enquetes-table">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Descricao</th>
            <th>Data Inicio</th>
            <th>Data Fim</th>
            <th>Ativo</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($enquetes as $enquete)
        <tr>
            <td>{!! $enquete->titulo !!}</td>
            <td>{!! $enquete->descricao !!}</td>
            <td>{!! date("d/m/Y",strtotime($enquete->data_inicio)) !!}</td>
            <td>{!! date("d/m/Y",strtotime($enquete->data_fim)) !!}</td>
            <td>{!! $enquete->ativo ? "SIM" : "NÃO" !!}</td>
            <td>
                {!! Form::open(['route' => ['enquetes.destroy', $enquete->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('enquetes.show', [$enquete->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('enquetes.edit', [$enquete->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>