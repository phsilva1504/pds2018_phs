<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Votar</title>

    <!-- Bootstrap Core CSS -->
    <link href="/votacao/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="/votacao/css/clean-blog.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/votacao/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{url('/blog')}}">Enquetes</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                     @if(Auth::check())
                        <li>
                            <a href="{{url('/home')}}">Meu painel</a>
                        </li>
                    @else
                        <li>
                            <a href="{{url('login')}}">Login</a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('/votacao/img/about-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="post-heading">
                        @include('flash::message')
                        <h1>{{$enquete->titulo}}</h1>
                        <span class="meta">Postado por {{$enquete->user->name}} on {{date('d/m/Y',strtotime($enquete->created_at))}}</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <p>{{$enquete->descricao}}</p>
                    {!! Form::open(['route' => 'votos.store']) !!}
                    <input type="hidden" value="{{$slug}}" name="slug">
                    <input type="hidden" value="{{$enquete->id}}" name="enquete_id">
                    @foreach($respostas as $resposta)
                        <div class="row">
                            <input type="radio" name="resposta_id" value="{{$resposta['id']}}">
                            {{$resposta['resposta'].' ('.$resposta['percentual'].'%)'}}
                        </div>
                    @endforeach
                    <br>
                     <div class="row">
                           Total de votos: {{$enquete->votos->count()}}
                        </div>
                    <input type="submit" value="Votar" class="btn btn-default">
                    {!! Form::close() !!}                    
                </div>
                
            </div>
        </div>
    </article>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    
                    <p class="copyright text-muted">Copyright &copy; Enquetes 2018</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="/votacao/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/votacao/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="/votacao/js/jqBootstrapValidation.js"></script>
    <script src="/votacao/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="/votacao/js/clean-blog.min.js"></script>

</body>

</html>
