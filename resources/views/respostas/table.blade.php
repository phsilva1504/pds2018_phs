<table class="table table-responsive" id="respostas-table">
    <thead>
        <tr>
            <th>Resposta</th>
            <th colspan="3">Ações</th>
        </tr>
    </thead>
    <tbody>
    @foreach($respostas as $resposta)
        <tr>
            <td>{!! $resposta->resposta !!}</td>
            <td>
                {!! Form::open(['route' => ['respostas.destroy', $resposta->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    
                   
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>