<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $resposta->id !!}</p>
</div>

<!-- Resposta Field -->
<div class="form-group">
    {!! Form::label('resposta', 'Resposta:') !!}
    <p>{!! $resposta->resposta !!}</p>
</div>

<!-- Enquete Id Field -->
<div class="form-group">
    {!! Form::label('enquete_id', 'Enquete Id:') !!}
    <p>{!! $resposta->enquete_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $resposta->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $resposta->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $resposta->deleted_at !!}</p>
</div>

