<div class="col-md-6">
<!-- Resposta Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('resposta', 'Resposta:') !!}
    {!! Form::textarea('resposta', null, ['class' => 'form-control']) !!}
</div>

<!-- Enquete Id Field -->
<div class="form-group col-sm-6">
    {!! Form::hidden('enquete_id', $enquete->id, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
</div>
</div>