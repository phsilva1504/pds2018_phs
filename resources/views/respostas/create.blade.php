@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Resposta
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
               
                    {!! Form::open(['route' => 'respostas.store']) !!}
                            @include('enquetes.show_fields')
                            @include('respostas.fields')
                    {!! Form::close() !!}

                    <div class="box-body">
                        @include('respostas.table')
                    </div>
                
            </div>
        </div>
    </div>
@endsection
