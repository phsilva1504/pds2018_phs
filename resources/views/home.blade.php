@extends('layouts.app')
@section('scripts')
	<script type="text/javascript">
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
      	var data = google.visualization.arrayToDataTable({!! $grafico !!}) // CSS-style declaration
        // Set chart options
        var options = {'title':'Votos em minhas enquetes',
                       'chartArea': {width: '50%'},
                       hAxis: {
				          title: 'Votos',
				          minValue: 0
				        }
                   };
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row">
    	<h1> Bem vindo , {!! Auth::user()->name !!}. </h1>
    	<br>
      @if($check)
    	<div align="center">
    		<div id="chart_div"></div>
    	</div>
      @endif
    </div>
</div>
@endsection
